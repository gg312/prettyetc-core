#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test field classes.
"""

# pylint: disable=E1101, E1120, W0703
import pytest
from hypothesis import given
from hypothesis.strategies import booleans
from hypothesis.strategies import data as draw_data
from hypothesis.strategies import integers, lists, none, one_of, text

from prettyetc.etccore.langlib import (
    DictField, IndexableField, ReadonlyException)

from .common import (
    DEFAULT_DATA, DICT_PRIMITIVES, FIELDTYPE_SELECT, INDEXABLE_DATA,
    PRIMITIVES_NO_COLLECTIONS, BaseTest, create_field)


class TestField(BaseTest):
    """Test field class."""

    # def setup_method(self, test_method):
    #     """Init test"""

    # def teardown_method(self, test_method):
    #     """Cleanup test"""

    @given(
        name=text(),
        data=DEFAULT_DATA,
        description=text(),
        attributes=none() | DICT_PRIMITIVES,
        readonly=booleans())
    def test_init_good(self, name, data, description, attributes, readonly):
        """Creating Field objects using good parameters."""
        self.create_field(
            name,
            data,
            description,
            attributes,
            readonly,
        )

    @given(
        name=text(),
        data=DEFAULT_DATA,
        description=text(),
        attributes=PRIMITIVES_NO_COLLECTIONS,
        readonly=DICT_PRIMITIVES | PRIMITIVES_NO_COLLECTIONS)
    def test_init_bad(self, name, data, description, attributes, readonly):
        """Creating Field objects using bad parameters."""
        field = self.create_field(
            name, data, description, attributes, readonly, fail=True)
        assert field is None

    @given(field=create_field(), new_data=DEFAULT_DATA)
    def test_write_data(self, field, new_data):
        """Creating Field objects using good parameters."""

        if field.readonly:
            with pytest.raises(ReadonlyException):
                field.data = new_data
                pytest.fail("Readonly check failed.")
        else:
            field.data = new_data
            assert field.data == new_data, "Data assignment failed."

    @given(
        field=create_field(readonly=False),
        new_field=create_field(readonly=False),
        pass_func=booleans())
    def test_dispatch(self, field, new_field, pass_func):
        """Test the Field event dispatcher."""
        # make dependency

        if pass_func:
            expected_events = {
                "name": new_field.name,
                "data": new_field.data,
                "description": new_field.description,
                "attributes": {} if new_field.attrs is None else new_field.attrs
            }
            received_events = {}

            def listener(value, valuetype):
                """Listen to dispatcher event."""

                # dispatcher should be called only once
                assert valuetype not in received_events
                received_events[valuetype] = value

            field.listener = listener

        field.name = new_field.name
        assert new_field.name == field.name

        field.data = new_field.data
        assert new_field.data == field.data

        field.description = new_field.description
        assert new_field.description == field.description

        field.attributes = new_field.attributes
        assert ({} if new_field.attributes is None else
                new_field.attributes) == field.attributes

        if pass_func:
            assert expected_events == received_events
            del field.listener
            assert [] == field.listener


class TestIndexable(BaseTest):
    """Test IndexableField and NestedField."""

    # def iterable_filter(field):
    #     """Accept only fields that data is list,tuple or dict."""
    #     return isinstance(field.data, (tuple, list, dict))

    @given(
        field=create_field(
            fieldtype=IndexableField,
            data=INDEXABLE_DATA,
            readonly=False,
        ),
        index=integers(min_value=0),
        new_field=create_field())
    def test_operators(self, field, index, new_field):
        """Check if data manipulating works."""
        data = field.data

        if isinstance(data, (list, tuple)):
            keys = tuple(range(len(data)))
        elif isinstance(data, dict):
            keys = tuple(data.keys())
        else:
            raise TypeError("Unsupported type for data parameter: {}".format(
                type(data).__name__))

        if index < len(keys):
            # get set and del item
            key = keys[index]
            old_val = field[key]
            assert old_val == data[key]

            field[key] = new_field
            assert new_field == data[key]

            del field[key]
            if isinstance(data, (list, tuple)):
                assert len(field) == len(keys) - 1
            elif isinstance(data, dict):
                with pytest.raises(KeyError):
                    key = keys[index]
                    _ = field[key]
                    pytest.fail("Unhandled bad key.")

        else:
            # error on getting item
            with pytest.raises((KeyError, IndexError)):
                key = keys[index]
                _ = field[key]
                pytest.fail("Unhandled bad key.")

    @given(field=create_field(
        fieldtype=IndexableField,
        data=INDEXABLE_DATA,
    ))
    def test_iter(self, field):
        """Check iter integrity."""

        for field_elm, data_elm in zip(field, field.data):
            assert field_elm == data_elm

    @given(
        field=one_of(
            create_field(
                fieldtype=IndexableField,
                data=lists(PRIMITIVES_NO_COLLECTIONS),
            ),
            create_field(
                fieldtype=DictField,
                data=DICT_PRIMITIVES,
            ),
        ))
    def test_iteritems(self, field):
        """Check iteritems integrity."""
        data_iterator = field.data.items() if isinstance(
            field.data, dict) else enumerate(field.data)
        for field_elm, elm in zip(field.iteritems(), data_iterator):
            assert field_elm == elm


class TestTyped(BaseTest):
    """Test type checking."""

    @given(
        data=draw_data(),
        fieldtype=FIELDTYPE_SELECT,
        good=booleans(),
    )
    def test_init(self, data, fieldtype, good):
        """Test if type checking are done in __init__."""

        fieldtype, strategy = fieldtype
        datatype = fieldtype.__TYPEOBJ__

        if good:
            try:
                field = data.draw(
                    create_field(
                        fieldtype=fieldtype,
                        data=strategy,
                    ))
                assert isinstance(field.data, datatype)
            except Exception:
                assert False, "Failed to create field from field type {}".format(
                    fieldtype.__name__)
        else:
            # IntField data can be a bool and vice-versa
            # because bool is a subclass of int.
            with pytest.raises(TypeError):
                field = data.draw(
                    create_field(
                        fieldtype=fieldtype,
                        data=DEFAULT_DATA.filter(
                            lambda x: not isinstance(x, datatype))))
                pytest.fail("Type controls not working.")

    @given(
        data=draw_data(),
        fieldtype=FIELDTYPE_SELECT,
        good=booleans(),
    )
    def test_set(self, data, fieldtype, good):
        """Test if type checking are done in property setters."""

        fieldtype, strategy = fieldtype
        datatype = fieldtype.__TYPEOBJ__

        field = self.create_field(
            "", None, "", {}, False, fieldtype=fieldtype, checks=False)

        assert field is not None

        if good:
            data = data.draw(strategy)
            try:
                field.data = data
            except TypeError:
                pytest.fail("Failed to create field from field type {}".format(
                    fieldtype.__name__))
            assert isinstance(field.data, datatype)

        else:
            # IntField data can be a bool and vice-versa
            # because bool is a subclass of int.
            fielddata = data.draw(
                DEFAULT_DATA.filter(lambda x: not isinstance(x, datatype)))
            with pytest.raises(TypeError):
                field.data = fielddata
                pytest.fail("Type controls not working.")
