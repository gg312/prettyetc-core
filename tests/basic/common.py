#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Common classes and composites for basic tests for fields.
"""

# pylint: disable=W0703, E1120
import re

import pytest
from hypothesis.strategies import (
    booleans, composite, dictionaries, floats, from_regex, integers, lists,
    none, one_of, recursive, sampled_from, text)
from prettyetc.etccore.langlib import (
    ArrayField, BoolField, DictField, Field, FloatField, IndexableField,
    IntField, RootField, StringField)

__all__ = (
    "INDENTIFIERS_ONLY",
    "PRIMITIVES_NO_COLLECTIONS",
    "DICT_PRIMITIVES",
    "DEFAULT_DATA",
    "INDEXABLE_DATA",
    "FIELDTYPE_SELECT",
    "create_field",
    "create_dictfield",
    "create_field_tree",
    "BaseTest",
)

INDENTIFIERS_ONLY = from_regex(
    re.compile(r"[a-zA-Z_][a-zA-Z0-9_]*"),
    fullmatch=True,
)

# hypothesis costants generators
PRIMITIVES_NO_COLLECTIONS = one_of(
    booleans(),
    text(),
    integers(),
    floats(allow_nan=False),
)

DICT_PRIMITIVES = dictionaries(text(), PRIMITIVES_NO_COLLECTIONS)
DEFAULT_DATA = one_of(
    booleans(),
    text(),
    integers(),
    floats(allow_nan=False),
    lists(PRIMITIVES_NO_COLLECTIONS),
    DICT_PRIMITIVES)

INDEXABLE_DATA = one_of(lists(PRIMITIVES_NO_COLLECTIONS), DICT_PRIMITIVES)

TYPED_STRATEGIES = (
    (StringField, text()),
    (IntField, integers()),
    (FloatField, floats()),
    (BoolField, booleans()),
    (ArrayField, lists(PRIMITIVES_NO_COLLECTIONS)),
    (DictField, DICT_PRIMITIVES),
)

FIELDTYPE_SELECT = sampled_from(TYPED_STRATEGIES)

FIELD_ATTRIBUTE_SELECT = sampled_from((
    "name",
    "data",
    "description",
    "attributes",
    "readonly",
))


@composite
def create_field(draw,
                 fieldtype=Field,
                 data=DEFAULT_DATA,
                 readonly=None,
                 type_by_data=False):
    """Create a random Field object."""
    if readonly is None:
        readonly = draw(booleans())

    data = draw(data)
    if type_by_data:
        datatype = type(data)
        for fieldcls, _ in TYPED_STRATEGIES:
            if datatype == fieldcls.__TYPEOBJ__:
                fieldtype = fieldcls
                break
    field = fieldtype(
        name=draw(text()),
        data=data,
        description=draw(text()),
        attributes=draw(none() | DICT_PRIMITIVES),
        readonly=readonly,
    )

    return field


@composite
def create_dictfield(draw, readonly=None, data=PRIMITIVES_NO_COLLECTIONS):
    """Create a dict field with dict keys corresponding to field name."""
    field_list = draw(lists(data))

    dict_data = {}
    for field in field_list:
        dict_data[field.name] = field

    field = DictField(
        name=draw(text()),
        data=dict_data,
        description=draw(text()),
        attributes=draw(none() | DICT_PRIMITIVES),
        readonly=readonly,
    )

    return field


@composite
def create_field_tree(draw, max_leaves=100) -> IndexableField:
    """Create a field tree."""

    def _tree_step(children):
        return create_field(
            fieldtype=ArrayField,
            data=lists(children),
        ) | create_dictfield(data=children)

    step = create_field(
        data=PRIMITIVES_NO_COLLECTIONS,
        type_by_data=True,
    )
    tree = draw(
        recursive(
            step,
            _tree_step,
            max_leaves=max_leaves,
        ).filter(lambda x: hasattr(x, "__iter__")))

    if isinstance(tree.data, IndexableField):
        tree.data = tree.data.data
    return tree


@composite
def create_tree_and_samples(draw, n_samples=1, max_leaves=100,
                            max_samples=5) -> (RootField, list):
    """
    Create a field tree and take n_samples from tree.

    If n_samples is None, an hypothesis integers will be drawn.
    """

    def _recursive_walker(field: Field):
        if isinstance(field, IndexableField):
            j = 0
            fieldlen = len(field)
            while j == 0:

                j = draw(integers(
                    min_value=0,
                    max_value=fieldlen + 1,
                ))

                if j == 0 and field not in samples:
                    return field

            j -= 1
            for i, child in enumerate(field):
                if i == j:
                    return _recursive_walker(child)
        return field

    tree = draw(create_field_tree(max_leaves=max_leaves))

    if n_samples is None:
        # max_value can be changed
        n_samples = draw(integers(
            min_value=1,
            max_value=max_samples,
        ))

    samples = []
    for _ in range(n_samples):
        samples.append(_recursive_walker(tree))

    return tree, samples


class BaseTest(object):
    """Basic test class for Fields."""

    def create_field(self,
                     name,
                     data,
                     description,
                     attributes,
                     readonly,
                     fail=False,
                     checks=True,
                     fieldtype=Field):
        """Create and assert fields."""
        if fail:
            with pytest.raises(TypeError):
                field = fieldtype(
                    name,
                    data,
                    description,
                    attributes,
                    readonly,
                )
                pytest.fail("Unhandled bad values")

            return None
        try:
            field = fieldtype(
                name=name,
                data=data,
                description=description,
                attributes=attributes,
                readonly=readonly,
            )
        except Exception:
            pytest.fail("Failed to create a {} object.".format(
                fieldtype.__name__))
            return None
        else:
            if checks:
                assert name == field.name
                assert data == field.data
                assert description == field.description
                assert ({} if attributes is None else
                        attributes) == field.attributes
                assert readonly == field.readonly
        return field
