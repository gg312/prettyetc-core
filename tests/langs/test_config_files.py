#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Test file parsing."""

import logging
import os

from hypothesis import given
from hypothesis.strategies import sampled_from
from prettyetc.etccore import ConfigFile, ConfigFileFactory, LoggerCreator


def setup_module(_):
    """Setup the logger."""
    _ = LoggerCreator(
        logging.getLogger("prettyetc"),
        logfile=None,
        log_level=logging.DEBUG,
    )
    assert LoggerCreator.ROOT_LOGGER != logging.root, "Failed to create logger"


class TestConfigFile(object):
    """Test ConfigFile through config file factory."""
    factory = ConfigFileFactory()

    @given(sampled_from(os.listdir("testconf")))
    def test_configfile_factoring_good(self, file):
        """Create a config file from factory."""
        conffile = self.factory(os.path.join("testconf", file))
        assert isinstance(conffile, ConfigFile)
