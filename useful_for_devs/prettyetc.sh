#!/bin/bash
# This script should be used only in *nix distros that have system qt libraries with plugins
# and PySide2 is installed via pip. In another contexts this script should not be used
#`or can create some problems.
# Especially if KDE is running, running prettyetc without this script create some glitches
# and use an ugly style (fusion).

# find plugin path
if [[ -n /usr/lib/x86_64-linux-gnu/qt5/plugins/ ]]; then
    plugin_path="/usr/lib/x86_64-linux-gnu/qt5/plugins"
fi

if [[ -z $plugin_path ]]; then
    python3 prettyetc_qt.py
else
    echo "$plugin_path/platforms"
    QT_DEBUG_PLUGINS=1 PYSIDE_DISABLE_INTERNAL_QT_CONF=1 python3 prettyetc_qt.py -platformpluginpath "$plugin_path/platforms" "$@"
fi
