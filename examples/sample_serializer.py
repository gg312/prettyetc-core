#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sample serializer (with a convenient structure).

This code is only an example for understanding how to implement serializer plugins.
"""

import io

# all of these classes can be imported in the package namespace
# For example:
# from prettyetc.etccore.langlib import Field, BaseSerializer, RootField
from prettyetc.etccore.langlib.field import (
    BoolField, Field, FloatField, IndexableField, IntField, NameField,
    StringField)
from prettyetc.etccore.langlib.serializers import (
    BaseSerializer, SerializeFailed)

"""
All class names that you want to expose to the plugin system must be contained in __all__

See https://www.python.org/dev/peps/pep-0008/#id50 for more information about __all__
"""
__all__ = ("SampleSerializer",)


class SampleNode(object):
    """
    An example element of a tree-based language.

    SampleNode children can be a python primitive or a SampleNode.
    """

    nodename = ""
    nodeattrs = {}
    nodedescription = ""
    # data can be a simple type
    nodedata = ""

    # or a collection
    # nodedata = []

    def __iter__(self):
        """Iterate node's children."""
        if hasattr(self.nodedata, "__iter__"):
            for child in self.nodedata:
                yield child
        else:
            raise TypeError("Data is not iterable.")


class SampleError(Exception):
    """A sample error for SampleSerializer and InternalSampleSerializer."""

    # where the error occurred (column and line, depends on the parser)
    ex_line = None
    ex_col = None

    ex_reason = ""
    ex_bad_string = None


class InternalSampleSerializer(object):
    """A sample implementation of an ideal language serializer
    that converts Field objects or a tree of them to pythonic objects.

    :class:`InternalSampleParser` is a class used as an internal parser,
    it makes use of 3rd party libraries if necessary, its main function is to
    offload some code from the main parser of the language,
    it is used in :class:`SampleParser`.

    This class is optional, you can directly use :class:`SampleParser`
    even if you need to use 3rd party libraries.
    (Note that by using InternalSampleParser you need to follow this flow:
        Raw --InternalSampleParser-> pythonic objects --SampleParser-> Field objects or a tree of them,
        where a pythonic object can be a custom defined class like SampleNode,
    If you instead don't create/use InternalSampleParser, you need to follow this flow:
        Raw --SampleParser-> Field objects or a tree of them).
    """

    def __init__(self, *args, **kwargs):
        # do some init
        super().__init__()
        self.args = args
        self.kwargs = kwargs

    def dump_string(self, node):
        """Dump a string of text from a python object or a collection of them."""
        # process the python object
        # return string of text


class SampleSerializer(BaseSerializer):
    """A sample implementation of an ideal language serializer
    (that implements :class:`~.BaseSerializer`),
    it converts pythonic objects to an ASCII text string.

    This class can make use of :class:`InternalSampleSerializer`,
    where some code for the serializing is offloaded.
    """

    # required properties
    LANGUAGES = ("sample",)
    STANDARD_EXTENSION = ".sample"

    # the loggername is optional if you don't use the integrated logger
    loggername = "sample"

    def serialize_field(self, field: Field) -> SampleNode:
        """
        Convert a :class:`~prettyetc.etccore.langlib.field.Field` object
        into a specific python object.

        Here is a simple implementation for recursive SampleNode processing.

        The explaination of the parameters can be found in the prettyetc's docs,
        in the API glossary.
        """
        node = SampleNode()
        node.nodename = str(field.name)
        node.nodedescription = str(field.description)
        node.nodeattrs = field.attributes
        if isinstance(field, IndexableField):
            node.nodedata = []
            for child in field:
                node.nodedata.append(self.serialize_field(child))

            # The tree lines above can be written using a list comprehension
            # node.nodedata = [self.serialize_field(x) for x in field]

        elif isinstance(field, (BoolField, StringField, IntField, FloatField)):
            node.nodedata = field.data
        elif isinstance(field, NameField):
            node.nodedata = None
        else:
            raise SerializeFailed(
                langname=self.LANGUAGES[0],
                reason="Can't serialize {}, unsupported type.".format(
                    type(field.data).__name__))

        return node

    def serialize_string(self, field: Field, stream: io.IOBase = None,
                         **_) -> str:
        if stream is None:
            _stream = io.StringIO()

        serializer = InternalSampleSerializer()
        root_node = self.serialize_field(field)
        serializer.dump_string(root_node)
