#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sample parser (with a convenient structure).

This code is only an example for understanding how to implement parser plugins.
It does nothing!
"""

import io

# all of these classes can be imported in the package namespace
# For example:
# from prettyetc.etccore.langlib import Field, BaseParser, RootField
from prettyetc.etccore.langlib.field import (
    ArrayField, BoolField, Field, FloatField, IntField, NameField, StringField)
from prettyetc.etccore.langlib.parsers import BadInput, BaseParser
from prettyetc.etccore.langlib.root import RootField

"""
All class names that you want to expose to the plugin system must be contained in __all__

See https://www.python.org/dev/peps/pep-0008/#id50 for more information about __all__
"""
__all__ = ("SampleParser",)


class SampleNode(object):
    """
    An example element of a tree-based language.

    SampleNode children can be a python primitive or a SampleNode.
    """

    nodename = ""
    nodeattrs = {}
    nodedescription = ""
    # data can be a simple type
    nodedata = ""

    # or a collection
    # nodedata = []

    def __iter__(self):
        """Iterate node's children."""
        if hasattr(self.nodedata, "__iter__"):
            for child in self.nodedata:
                yield child
        else:
            raise TypeError("Data is not iterable.")


class SampleLangError(Exception):
    """A sample error for SampleLangParser."""

    # where the error occurred (column and line, depends on the parser)
    ex_line = None
    ex_col = None

    ex_reason = ""
    ex_bad_string = None


class InternalSampleParser(object):
    """A sample implementation of an ideal language parser
    that converts an ASCII text string to pythonic objects.

    :class:`InternalSampleParser` is a class used as an internal parser,
    it makes use of 3rd party libraries if necessary, its main function is to
    offload some code from the main parser of the language,
    it is used in :class:`SampleParser`.
    """

    def __init__(self, *args, **kwargs):
        # do some init
        super().__init__()
        self.args = args
        self.kwargs = kwargs

    def load_string(self, string):
        """Load a string of text and return a python object or a collection of them."""
        # process the string
        # return element


class SampleParser(BaseParser):
    """A sample implementation of an ideal language parser (that implements BaseParser),
    it converts pythonic objects to Field objects or a tree of them.

    This class makes use of :class:`SampleParser`, where some code for the parsing
    is offloaded.
    """

    # required properties
    LANGUAGES = ("sample",)
    # optional properties
    PREFIXES = ("sample",)
    SUFFIXES = (".sample",)

    # the loggername is optional if you don't use the integrated logger
    loggername = "sample"

    def parse_field(self,
                    name,
                    data,
                    description: str = "",
                    readonly: bool = False,
                    **attributes) -> Field:
        """
        Parse a field represented as a specific python object to
        :class:`~prettyetc.etccore.langlib.field.Field`

        Here is a simple implementation for recursive SampleNode processing.

        The explaination of the parameters can be found in the prettyetc's docs,
        in the API glossary.
        """

        if data is None:
            return NameField(
                name, description=description, attributes=attributes)

        if isinstance(data, bool):
            return BoolField(name, data=data, description=description)

        if isinstance(data, str):
            return StringField(
                name, data=data, description=description, attributes=attributes)

        if isinstance(data, int):
            return IntField(
                name, data=data, description=description, attributes=attributes)

        if isinstance(data, float):
            return FloatField(
                name, data=data, description=description, attributes=attributes)

        if isinstance(data, SampleNode):
            try:
                # create an empty ArrayField
                field = ArrayField(
                    name,
                    data=[],
                    description=description,
                    attributes=attributes)
                # try to iter children, using __iter__
                # remember that the for statement can iterate data
                # instead of data.nodedata directly
                for node in data:
                    field.append(
                        self.parse_field(
                            node.nodename,
                            node.nodename,
                            description=node.nodedescription,
                            **node.attributes))

                return field

            except TypeError:
                return self.parse_field(
                    data.nodename,
                    data.nodename,
                    description=data.nodedescription,
                    **data.attributes)

        raise TypeError("Unsupported {} type".format(type(data).__name__))

    def parse_string(self, string):
        """Parse the given string using SampleLangParser.load_string."""

        parser = InternalSampleParser()
        try:
            root = parser.load_string(string)  # pylint: disable=E1111
        except SampleLangError as ex:
            # the langname parameter should be contained in LANGUAGES attrubute
            raise BadInput(
                langname="sample",
                line=ex.ex_line,
                column=ex.ex_col,
                reason=ex.ex_reason,
                incriminated_string=ex.ex_bad_string,
                original_exc=ex)

        except TypeError as ex:
            raise BadInput(langname="sample", original_exc=ex)

        res = []
        try:
            # try to iter children, using __iter__
            # remember that the for statement can iterate data.nodedata
            # instead of data directly
            for node in root:
                res.append(
                    self.parse_field(
                        node.nodename,
                        node.nodedata,
                        description=node.nodedescription,
                        **node.attributes))

        except TypeError:
            # Data is not iterable.
            # This try block is specific for the sample implementation
            # you probably don't need that
            res = [
                self.parse_field(
                    root.nodename,
                    root.nodedata,
                    description=root.nodedescription,
                    **root.nodeattrs)
            ]
        # rootfield data should be an iterable,
        # non iterable data can create problems in ui.
        rootfield = RootField(
            "root",
            data=res,
            langname="sample",
            # description=some_description,
            # **some_metadata
        )
        return rootfield

    # BaseParser parse_file example
    def parse_file_baseparser(self, stream: open, **kwargs) -> RootField:
        """
        Read a file; by default it read single lines.

        :raises :class:`BadInput`: If an error occurred while
                                   parsing the content of the stream.

        :raises IOError: If an error occurred while reading
                         the stream or the stream is not a valid readable stream.
        """
        fields = []
        i = None
        if isinstance(stream, io.TextIOWrapper) and stream.readable():
            try:
                for i, line in enumerate(stream):
                    fields.append(self.parse_line(line, **kwargs))

            except UnicodeDecodeError as ex:
                raise BadInput(
                    filename=stream.name,
                    # assuming that is a binary data, so there are no lines.
                    column=range(ex.start, ex.end),
                    line=i,
                    reason=ex.reason,
                    langname=self.LANGUAGES[0],
                    original_exc=ex)

            except BadInput as ex:
                ex.filename = stream.name
                ex.line = i
                raise ex
            root = RootField(
                "root", typeconf="ini", data=fields, name=stream.name)
            return root
        raise IOError("Given stream isn't readable.")

    # DictParser parse_file example
    def parse_file_dictparser(self, stream: open, **kwargs):
        """Read all content of a file-like object, must be readable."""
        if isinstance(stream, io.TextIOWrapper) and stream.readable():
            try:
                root = self.parse_string(stream.read(), **kwargs)
                root.name = stream.name

            except UnicodeDecodeError as ex:
                raise BadInput(
                    filename=stream.name,
                    # assuming that is a binary data, so there are no lines.
                    column=range(ex.start, ex.end),
                    reason=ex.reason,
                    langname=self.LANGUAGES[0],
                    original_exc=ex)
            except BadInput as ex:
                ex.filename = stream.name
                raise ex
            return root
        raise IOError("Given stream isn't readable.")
