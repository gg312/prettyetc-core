The baseui.settings module
==========================

.. if there are subpackages of this package
..
.. Subpackages
.. -----------
..
 .. toctree::
..
..    subpackages
..
..
.. Submodules
.. ----------
..
 .. toctree::
..
..     submodules
..
.. Module content
.. --------------




.. automodule:: prettyetc.baseui.settings
   :members:
   :show-inheritance:
