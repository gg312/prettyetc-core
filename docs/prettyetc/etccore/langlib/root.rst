The etccore.langlib.root module
===================================

.. if there are subpackages of this package
..
.. Subpackages
.. -----------
..
 .. toctree::
..
..    subpackages
..
..
.. Submodules
.. ----------
..
 .. toctree::
..
..     submodules
..
.. Module content
.. --------------


.. automodule:: prettyetc.etccore.langlib.root
   :members:
   :show-inheritance:
   :special-members:
