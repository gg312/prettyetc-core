The etccore.plugins module
==========================

.. if there are subpackages of this package
..
.. Subpackages
.. -----------
..
 .. toctree::
..
..    subpackages
..
..
.. Submodules
.. ----------
..
 .. toctree::
..
..     submodules


.. automodule:: prettyetc.etccore.plugins
   :members:
   :show-inheritance:
