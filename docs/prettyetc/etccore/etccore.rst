The etccore package
===================

Subpackages
-----------

.. toctree::
   :titlesonly:

   langlib/langlib
   langs/langs


Submodules
----------

.. toctree::
    :titlesonly:

    confmgr
    logger
    plugins


=======
Etccore
=======

.. automodule:: prettyetc.etccore
