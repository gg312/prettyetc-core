All prettyetc versions
======================

All documentation versions and repository branch are listed here.

master branch
    - sources: `<https://gitlab.com/prettyetc/prettyetc-core>`_
    - docs: `<https://prettyetc.gitlab.io/prettyetc-core>`_

0.3.x
    - sources: `<https://gitlab.com/prettyetc/prettyetc-core/tree/0.3.x>`_
    - docs: `<https://prettyetc.gitlab.io/prettyetc-core/0.3.x>`_

0.2.x
    - sources: `<https://gitlab.com/prettyetc/prettyetc-core/tree/0.2.x>`_
    - docs: `<https://prettyetc.gitlab.io/prettyetc-core/0.2.x>`_

0.1.x
    - sources: `<https://gitlab.com/prettyetc/prettyetc-core/tree/0.1.x>`_
    - docs: `<https://prettyetc.gitlab.io/prettyetc-core/0.1.x>`_
