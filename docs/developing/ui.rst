===================================================================
How to develop a launchable UI with prettyetc's UI dynamic launcher
===================================================================

Introduction
************

When we talking about configuration files, we can't refer to
an interface to view, manage or edit them.
The prettyetc baseui module allows you to develop quickly an UI,
automating and abstracting some stuffs required to use the prettyetc
core library, but also providing other useful stuffs, as
the :meth:`~prettyetc.baseui.ui.main.BaseMain.read_file` method and
the :meth:`~prettyetc.baseui.ui.main.BaseMain.write_file` method, for example.


:class:`~prettyetc.baseui.ui.main.BaseMain` subclassing
*******************************************************

The :class:`~prettyetc.baseui.ui.main.BaseMain` class, as most classes in the
:mod:`~prettyetc.baseui.ui` module, has a lifecycle, that must be implemented
through 3 methods to be overridden. Each methods represent a phase in the object lifecycle.

Here are the 3 phases and them methods:

- init phase (not called in \_\_init\_\_)
  :meth:`~prettyetc.baseui.ui.common.CommonComponent.init_ui` (optional)

- show phase (must be a blocking call)
  :meth:`~prettyetc.baseui.ui.common.CommonComponent.show`    (required)

- close phase (after this phase the instance is consider dead)
  :meth:`~prettyetc.baseui.ui.common.CommonComponent.close`   (required)

Next, the :class:`~prettyetc.baseui.ui.main.BaseMain` subclass needs a name,
not necessary unique in the launcher, that identify the UI.
You should set it in the class-level attribute :attr:`~prettyetc.baseui.ui.uiname`.

So your code should have this structure.

.. code-block:: python3

    class MyMain(BaseMain):

        uiname = "myuiname"

        def init_ui():
            # some initializing

        def show():
            # some blocking code

        def close():
            # some cleanup code


:func:`~prettyetc.baseui.ui.main.generate_main`: Another way to create :class:`~prettyetc.baseui.ui.main.BaseMain` subclasses
*****************************************************************************************************************************

If you don't want to use the whole :mod:`~prettyetc.baseui` module,
but you need to have all core stuffs initialized or you just don't want subclass,
then you can use a class generator to create
the needed :class:`~prettyetc.baseui.ui.main.BaseMain` subclass.

However, the :func:`~prettyetc.baseui.ui.main.generate_main` does not implement implicity the
:class:`~prettyetc.baseui.ui.main.BaseMain` lifecycle (except for the close phase) and the UI name,
so you should provide at least a callable for the show phase and the name.
Remember that :func:`~prettyetc.baseui.ui.main.generate_main` returns a :class:`~type`,
so a class, not an instance of that.
