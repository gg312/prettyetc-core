=======================================
How to develop a language parser plugin
=======================================

Here's a detailed step-by-step guide to create a parser plugin file.


1. create a Python file like this:

    .. code::

        from prettyetc.etccore.langlib.parser import BaseParser

        __all__ = ("LangParser", )

        class LangParser(BaseParser):
            # some_metadata
            LANGUAGES = ("lang", "lang2")

            # some_meth
            def parse_string(self, string):
                # some code

    In this snippet we use
    :class:`~prettyetc.etccore.langlib.parsers.BaseParser`,
    but you can use also provided subclasses
    of :class:`~prettyetc.etccore.langlib.parsers.BaseParser`.


2. Add the metadata information:

  - **loggername**
      Set the logger name in logging syntax
      (if you want to use integrated logger).

  - **LANGUAGES**
       A tuple of languages that the parser provides.
       Languages must be lowercase.

  - **PREFIXES**
       A tuple of prefixes that the acceptable files can have,
       used by the file matcher.

  - **SUFFIXES**
       A tuple of suffixes that the acceptable files can have,
       used by the file matcher.

3. Implements the abstract methods

  You can parse a config using in different ways.

  - by parsing the whole file as a string

   (the common situation if you use an external library).

  - by parsing each line of a file (default way).
  - by parsing file using chunks of string.

   (probably no one use this way)

  This is the abstract methods list.


  - :meth:`~prettyetc.etccore.langlib.parsers.BaseParser.parse_field`
      A recursive method that create fields by given name,
      data, description (if available) and attributes (if available).

      The data can be a collection of fields that children
      should be converted to fields.

      .. seealso::
        Module :mod:`~prettyetc.etccore.langlib`
        fields definitions for more information.

  - :meth:`~prettyetc.etccore.langlib.parsers.BaseParser.parse_line`
      Parse a line of config
  - :meth:`~prettyetc.etccore.langlib.parsers.BaseParser.parse_string`
      Parse a string of config
  - :meth:`~prettyetc.etccore.langlib.parsers.BaseParser.parse_file`
      Parse a file-like stream of config,
      the given should have implemented the name attribute and the read method.


5. Creating the root field

  The root field must be an instance of :class:`~prettyetc.etccore.langlib.root.RootField`
  or a subclass of it that contains all the valid and processed fields in the config.

  By default (using :class:`~prettyetc.etccore.langlib.root.RootField`)
  represents its data as a :class:`~prettyetc.etccore.langlib.field.Field` tree and
  the root field data should be a collection of the top element of the tree.


.. seealso::
    A complete example of a parser plugin can be found in the git repo,
    in the file :doc:`examples/sample_parser.py <../examples/sample_parser>`

    .. versionadded:: 0.2.0
