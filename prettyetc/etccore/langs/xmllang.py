#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
==========
Xml plugin
==========

Module for parsing xml.

Supported features:

- string parsing
- file parsing

Unsupported features:

- string serializing
- file serializing (inherited from BaseSerializer)
- metadata
- readonly attribute
- description attribute
- nested fields
"""

import xml.etree.ElementTree as ET

import prettyetc.etccore.langlib as langlib
import prettyetc.etccore.langlib.parsers as base
from prettyetc.etccore.langlib import (
    ArrayField, Field, NameField, RootField, StringField)
from prettyetc.etccore.langlib.serializers import BaseSerializer
from prettyetc.etccore.plugins import PluginBase

__all__ = ("XMLParser", "XMLSerializer")


class XMLParser(base.DictParser, PluginBase):
    SUFFIXES = (".xml",)
    LANGUAGES = ("xml",)

    def parse_line(self, line: str):
        raise NotImplementedError("XML parsing by line is unsupported.")

    def parse_field(self,
                    name,
                    data,
                    description: str = "",
                    readonly: bool = False,
                    **attributes) -> Field:

        if data is None:
            return NameField(
                name, description=description, attributes=attributes)

        if isinstance(data, str):
            return StringField(
                name,
                data=data.strip(),
                description=description,
                attributes=attributes)

        if isinstance(data, ET.Element):
            try:
                field = ArrayField(
                    name,
                    data=[],
                    description=description,
                    attributes=attributes)

                if data.text is not None and data.text.strip():
                    field.append(self.parse_field(
                        "",
                        data.text,
                    ))

                for ref in data:
                    field.append(self.parse_field(ref.tag, ref, **ref.attrib))

                    if ref.tail is not None and ref.tail.strip():
                        field.append(self.parse_field(
                            "",
                            ref.tail,
                        ))

                return field

            except TypeError:
                return self.parse_field(data.tag, data)

        raise TypeError("Unsupported {} type".format(type(data).__name__))

    def parse_string(self, string: str):
        """Parse a xml file into fields."""
        res = []
        try:
            root = ET.fromstring(string)
            res.append(self.parse_field(root.tag, root))
        except Exception as ex:
            raise ex

        return langlib.RootField("root", typeconf="xml", data=res)

    def parse_file(self, stream: open, **kwargs):
        """The parse_file method."""
        try:
            root = super().parse_file(stream, **kwargs)
            return root
        except Exception as ex:
            raise base.BadInput(
                filename=stream.name, langname="xml", original_exc=ex)


class XMLSerializer(BaseSerializer):
    STANDARD_EXTENSION = ".xml"
    LANGUAGES = ("xml",)

    def serialize_field(self, field: Field):
        element = ET.Element("")

        if isinstance(field, RootField):
            for x in field:
                element = self.serialize_field(x)

        if isinstance(field, ArrayField):
            # Root Element
            element = ET.Element(field.name, attrib=field.attributes)

            if len(field) == 1:
                element.text = field[0].data
            else:
                for elem in field:
                    element.append(self.serialize_field(elem))
        if isinstance(field, StringField):
            element = ET.Element(field.name, attrib=field.attributes)
            element.text = field.data

        if isinstance(field, NameField):
            element = ET.Element(field.name, attrib=field.attributes)

        return element

    def serialize_string(self, field, **_):
        return ET.tostring(
            self.serialize_field(field), encoding='utf8',
            method='xml').decode()
