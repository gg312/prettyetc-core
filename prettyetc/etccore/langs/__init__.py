#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
================
Built-in plugins
================

This package contains all core parsers and serializers.


Avaiable language parsers:

- json
- ini
- etc
- xml

Avaiable language serializers:

- json
- ini


Totally unimplemented languages:

- yaml
- csv
"""
